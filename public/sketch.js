let cols, rows, grid;
const resolution = 15;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(30);

  cols = Math.floor(width / resolution);
  rows = Math.floor(height / resolution);

  grid = new Array(cols);
  for (let i = 0; i < cols; i++) {
    grid[i] = new Array(rows);
    for (let j = 0; j < rows; j++) {
      let x = i * resolution;
      let y = j * resolution;

      grid[i][j] = {
        x: i,
        y: j,
        live: Math.floor(random(2))
      };

      noStroke();
      if (grid[i][j].live) fill(0);
      else fill(255);
      rect(x, y, resolution, resolution, resolution / 4, resolution / 4);
    }
  }
}

function draw() {
  let tempGrid = new Array(cols);
  for (let i = 0; i < cols; i++) {
    tempGrid[i] = new Array(rows);
    for (let j = 0; j < rows; j++) {
      tempGrid[i][j] = grid[i][j].live;
    }
  }

  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      let state = tempGrid[i][j];
      let neighbors = countNeighbors(tempGrid, i, j, rows, cols);

      if (state === 0 && neighbors === 3) {
        fill(0);
        rect(
          grid[i][j].x * resolution,
          grid[i][j].y * resolution,
          resolution,
          resolution,
          resolution / 4,
          resolution / 4
        );
        grid[i][j].live = 1;
      } else if (state === 1 && (neighbors > 3 || neighbors < 2)) {
        fill(255);
        rect(
          grid[i][j].x * resolution,
          grid[i][j].y * resolution,
          resolution,
          resolution,
          resolution / 4,
          resolution / 4
        );
        grid[i][j].live = 0;
      }
    }
  }
}

const countNeighbors = (grid, x, y, rows, cols) => {
  let sum = 0;
  for (let i = -1; i < 2; i++) {
    for (let j = -1; j < 2; j++) {
      let posX = (x + i + cols) % cols;
      let posY = (y + j + rows) % rows;

      sum += grid[posX][posY];
    }
  }
  sum -= grid[x][y];
  return sum;
};
